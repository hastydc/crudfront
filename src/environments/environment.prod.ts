export const environment = {
  production: true,
  apiUrl: 'https://services-prod.worldoffice.cloud/'
};
