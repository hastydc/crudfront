import {Component, OnInit} from '@angular/core';
import {HttpClientService} from '../../services/http-client.service';

@Component({
  selector: 'app-list',
  styleUrls: ['./list.component.scss'],
  templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {
  teamList: any;

  constructor(
    private http: HttpClientService,
  ) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.httpGet('equipos/listar').subscribe(d => {
      this.teamList = d;
    });
  }

  delete(id) {
    this.http.httpDelete('equipos/eliminar/' + id).subscribe(d => {
      alert('El equipo se elimino exitosamente');
      this.getData();
    });
  }
}
