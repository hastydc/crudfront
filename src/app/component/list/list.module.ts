import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ListComponent} from './list.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ListRoutes} from './list-routing.module';

@NgModule({
  imports: [
    RouterModule.forChild(ListRoutes),
    CommonModule,
  ],
  declarations: [
    ListComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ListModule {}
