import {Route} from '@angular/router';
import {ListComponent} from './list.component';

export const ListRoutes: Route[] = [
  { path: '', component: ListComponent},
];
