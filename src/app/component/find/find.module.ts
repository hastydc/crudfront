import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FindComponent} from './find.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FindRoutes} from './find-routing.module';

@NgModule({
  imports: [
    RouterModule.forChild(FindRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    FindComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class FindModule {}
