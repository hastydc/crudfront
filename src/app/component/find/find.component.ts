import {Component, OnInit} from '@angular/core';
import {HttpClientService} from '../../services/http-client.service';

@Component({
  selector: 'app-find',
  styleUrls: ['./find.component.scss'],
  templateUrl: './find.component.html'
})

export class FindComponent implements OnInit {
  showOne: boolean;
  showList: boolean;
  teamList: any;

  constructor(
    private http: HttpClientService,
  ) {}

  ngOnInit() {
  }

  findTeamById() {
    this.showOne = true;
    this.showList = false;
    const id = (document.getElementById('id') as HTMLInputElement).value;

    this.http.httpGet('equipos/consultar/' + id).subscribe(d => {
      Object.keys(d).map(k => {
        (document.getElementById(k) as HTMLInputElement).value = d[k];
      });

    });
  }

  findTeamByDates() {
    this.showList = true;
    this.showOne = false;
    const startDate = (document.getElementById('startDate') as HTMLInputElement).value;
    const endDate = (document.getElementById('endDate') as HTMLInputElement).value;

    this.http.httpGet('equipos/consultar/' + startDate  + '/' + endDate).subscribe(d => {
      this.teamList = d;
    });
  }
}
