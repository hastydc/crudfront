import {Route} from '@angular/router';
import {FindComponent} from './find.component';

export const FindRoutes: Route[] = [
  { path: '', component: FindComponent},
];
