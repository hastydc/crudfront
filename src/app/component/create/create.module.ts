import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CreateComponent} from './create.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {CreateRoutes} from './create-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    RouterModule.forChild(CreateRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CreateComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CreateModule {}
