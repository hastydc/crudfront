import {Route} from '@angular/router';
import {CreateComponent} from './create.component';

export const CreateRoutes: Route[] = [
  { path: '', component: CreateComponent},
];
