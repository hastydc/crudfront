import {Component, OnInit} from '@angular/core';
import {HttpClientService} from '../../services/http-client.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  styleUrls: ['./create.component.scss'],
  templateUrl: './create.component.html'
})

export class CreateComponent implements OnInit {
  teamForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClientService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.teamForm = this.formBuilder.group({
      nombre: '',
      estadio: '',
      sitioWeb: '',
      nacionalidad: '',
      fundacion: '',
      entrenador: '',
      capacidad: 0,
      valor: 0
    });
  }

  save() {
    this.http.httpPost('equipos/crear', this.teamForm.controls).subscribe(d => {
      alert('El equipo se creo exitosamente');
      this.router.navigate(['/lista']);
    });
  }
}
