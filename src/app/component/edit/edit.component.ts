import {Component, OnInit} from '@angular/core';
import {HttpClientService} from '../../services/http-client.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit',
  styleUrls: ['./edit.component.scss'],
  templateUrl: './edit.component.html'
})

export class EditComponent implements OnInit {
  teamForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClientService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.buildForm();
        this.getTeam(params.id);
      }
    });
  }

  buildForm() {
    this.teamForm = this.formBuilder.group({
      nombre: '',
      estadio: '',
      sitioWeb: '',
      nacionalidad: '',
      fundacion: '',
      entrenador: '',
      capacidad: '',
      valor: '',
      id: ''
    });
  }

  getTeam(id) {
    const self = this;

    this.http.httpGet('equipos/consultar/' + id).subscribe(d => {
      Object.keys(d).map(k => {
        self.teamForm.get(k).setValue(d[k]);
      });
    });
  }

  save() {
    this.http.httpPut('equipos/actualizar/' + this.teamForm.get('id').value, this.teamForm.controls).subscribe(() => {
      alert('El equipo se edito exitosamente');
      this.router.navigate(['/lista']);
    });
  }
}
