import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {EditComponent} from './edit.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditRoutes} from './edit-routing.module';

@NgModule({
  imports: [
    RouterModule.forChild(EditRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    EditComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class EditModule {}
