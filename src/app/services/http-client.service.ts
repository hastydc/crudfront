import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from 'src/environments/environment';


@Injectable()
export class HttpClientService {
  options: any;

  constructor(
    private http: HttpClient,
  ) {
    this.options = {
      headers: new HttpHeaders({'Content-type': 'application/json'})
    };
  }

  public url(path: string): string {
    return  environment.apiUrl + path;
  }

  public formatParams(params) {
    const response  = new Object();

    Object.keys(params).map((k) => {
      response[k] = params[k].value;
    });

    return response;
  }

  public httpGet(path): Observable<any> {
    return this.http.get<Observable<any>>(this.url(path), {withCredentials: true})
      .pipe(
        map( data => {
          return data;
        }),
        catchError( error => {
          return throwError(error);
        })
      );
  }

  public httpPost(path, params): Observable<any> {
    return this.http.post<Observable<any>>(this.url(path), this.formatParams(params), this.options)
      .pipe(
        map( data => {
          return data;
        }),
        catchError( error => {
          return throwError(error);
        })
      );
  }

  public httpPut(path, params): Observable<any> {
    return this.http.put<Observable<any>>(this.url(path), this.formatParams(params), this.options)
      .pipe(
        map( data => {
          return data;
        }),
        catchError( error => {
          return throwError(error);
        })
      );
  }

  public httpDelete(path): Observable<any> {
    return this.http.delete<Observable<any>>(this.url(path))
      .pipe(
        map(data => {
          return data;
        }),
        catchError(error => {
          return throwError(error);
        })
      );
  }
}




