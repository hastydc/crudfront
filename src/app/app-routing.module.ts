import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', loadChildren: () => import('./component/list/list.module').then(m => m.ListModule)},
  {path: 'lista', loadChildren: () => import('./component/list/list.module').then(m => m.ListModule)},
  {path: 'crear', loadChildren: () => import('./component/create/create.module').then(m => m.CreateModule)},
  {path: 'editar/:id', loadChildren: () => import('./component/edit/edit.module').then(m => m.EditModule)},
  {path: 'buscar', loadChildren: () => import('./component/find/find.module').then(m => m.FindModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})

export class AppRoutingModule {}
